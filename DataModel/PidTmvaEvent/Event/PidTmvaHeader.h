#ifndef PidTmvaHeader_h
#define PidTmvaHeader_h

#include "Event/HeaderObject.h"
#include "Event/PidTmvaEvent.h"
#include "EDMUtil/SmartRef.h"
#include <string>

namespace nEXO
{
    class PidTmvaHeader: public HeaderObject
    {
        private:
            nEXO::SmartRef m_event; // ||

            //input
            double dx_max;
            double dy_max;
            double charge;
            int channel_num;
            int peak_num;
            double rising_time;
            //output
            double value;
            double value_err;


        public:
            PidTmvaHeader();
            ~PidTmvaHeader();

        public:
            const Long64_t getEventEntry() const {
                return m_event.entry();
            }
            EventObject* event() {
                return m_event.GetObject();
            }
            void setEvent(PidTmvaEvent* value) {
                m_event = value;
            }
            void setEventEntry(const std::string& eventName, Long64_t& value);
            EventObject* event(const std::string& eventName);
            bool hasEvent();

        public:

            // getter
            double get_dx_max() { return dx_max; }
            double get_dy_max() { return dy_max; }
            double get_charge() { return charge; }
            int    get_channel_num() { return charge; }
            int    get_peak_num() { return peak_num; }
            double get_rising_time() { return rising_time; }
            double get_value() { return value; }
            double get_value_err() { return value_err; }

            // setter
            void set_dx_max( double v ) { dx_max = v; }
            void set_dy_max( double v ) { dy_max = v; }
            void set_charge( double v ) { charge = v; }
            void set_channel_num( int v ) { channel_num = v; }
            void set_peak_num( int v ) { peak_num = v; }
            void set_rising_time( double v ) { rising_time = v; }
            void set_value( double v) { value = v; }
            void set_value_err( double v ) { value_err = v; }

            ClassDef(PidTmvaHeader,1)
    };
}
#endif
