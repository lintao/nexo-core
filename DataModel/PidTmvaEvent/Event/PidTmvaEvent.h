#ifndef PidTmvaEvent_h
#define PidTmvaEvent_h

#include "Event/EventObject.h"


namespace nEXO
{

  class PidTmvaEvent: public EventObject
  {
  private:


  protected:


  public:

    /// Default Constructor
    PidTmvaEvent() {}
  
    /// Default Destructor
    virtual ~PidTmvaEvent() {}
  
    ClassDef(PidTmvaEvent,1);
  

  }; 

} 

#endif 
