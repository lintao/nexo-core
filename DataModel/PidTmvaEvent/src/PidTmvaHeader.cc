
#include "Event/PidTmvaHeader.h"

ClassImp(nEXO::PidTmvaHeader);

namespace nEXO
{
    PidTmvaHeader::PidTmvaHeader() {
        dx_max = 0;
        dy_max = 0;
        charge = 0;

        channel_num = 0;
        peak_num = 0;
        rising_time = 0;

        value = 0;
        value_err = 0;
    }

    PidTmvaHeader::~PidTmvaHeader() {

    }

    void PidTmvaHeader::setEventEntry(const std::string& eventName, Long64_t& value) {
        if (eventName == "nEXO::PidTmvaEvent") {
            m_event.setEntry(value);
        }
    }

    EventObject* PidTmvaHeader::event(const std::string& eventName) {
        if (eventName == "nEXO::PidTmvaEvent") {
            return m_event.GetObject();
        }
        return 0;
    }

    bool PidTmvaHeader::hasEvent() {
        return m_event.HasObject();
    }
}
