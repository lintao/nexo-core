#ifndef EVTNAVIGATOR_NAV_BUFFER_H
#define EVTNAVIGATOR_NAV_BUFFER_H

#include "EvtNavigator/EvtNavigator.h"
#include "DataBuffer/DataBuffer.h"

namespace nEXO {

typedef DataBuffer<EvtNavigator> NavBuffer;

}

#endif
