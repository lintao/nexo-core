#include "EvtNavigator/EvtNavigator.h"
#include "EDMManager.h"
#include <algorithm>

ClassImp(nEXO::EvtNavigator);

nEXO::EvtNavigator::EvtNavigator()
{
    this->Class()->IgnoreTObjectStreamer();
}

nEXO::EvtNavigator::~EvtNavigator()
{
    std::vector<nEXO::SmartRef*>::iterator it, end = m_refs.end();
    for (it = m_refs.begin(); it != end; ++it) {
        delete *it;
    }
}

nEXO::EvtNavigator::EvtNavigator(const nEXO::EvtNavigator& nav)
    : TObject(nav)
{
    this->init(nav);
}

nEXO::EvtNavigator& nEXO::EvtNavigator::operator=(const nEXO::EvtNavigator& nav)
{
    if (this != &nav) {
        TObject::operator=(nav);
        this->init(nav);
    }
    return *this;
}

void nEXO::EvtNavigator::init(const nEXO::EvtNavigator& nav)
{
    m_paths = nav.m_paths;
    m_writeFlag = nav.m_writeFlag;
    m_TimeStamp = nav.m_TimeStamp;
    // Clear previous SmartRefs
    std::vector<SmartRef*>::iterator it, end = m_refs.end();
    for (it = m_refs.begin(); it != end; ++it) {
        delete *it;
    }
    // Copy new SmartRefs
    std::vector<SmartRef*>::const_iterator it2, end2 = nav.m_refs.end();
    for (it2 = nav.m_refs.begin(); it2 != end2; ++it2) {
        SmartRef* ref = new SmartRef(**it2);
        m_refs.push_back(ref);
    }
}

nEXO::HeaderObject* nEXO::EvtNavigator::getHeader(const std::string& path)
{
    nEXO::SmartRef* ref = this->getSmartRef(path);
    if (!ref) {
        return 0;
    }
    return static_cast<nEXO::HeaderObject*>(ref->GetObject());
}

std::vector<std::string>& nEXO::EvtNavigator::getPath()
{
    return m_paths;
}

const std::vector<std::string>& nEXO::EvtNavigator::getPath() const
{
    return m_paths;
}

std::vector<nEXO::SmartRef*>& nEXO::EvtNavigator::getRef()
{
    return m_refs;
}

const std::vector<nEXO::SmartRef*>& nEXO::EvtNavigator::getRef() const
{   
    return m_refs;
}

void nEXO::EvtNavigator::setHeaderEntry(const std::string& path, int entry)
{
    nEXO::SmartRef* ref = this->getSmartRef(path);
    if (!ref) {
        return;
    }
    ref->setEntry(entry);
}

void nEXO::EvtNavigator::addHeader(const std::string& path, nEXO::HeaderObject* header)
{
    m_paths.push_back(path);
    SmartRef* ref = new SmartRef();
    m_refs.push_back(ref);
    m_refs.back()->SetObject(header);
    m_writeFlag.push_back(true);
}

void nEXO::EvtNavigator::addHeader(nEXO::HeaderObject* header) 
{
    m_paths.push_back(EDMManager::get()->getPathWithHeader(header->ClassName()));
    SmartRef* ref = new SmartRef();
    m_refs.push_back(ref);
    m_refs.back()->SetObject(header);
    m_writeFlag.push_back(true);
}

void nEXO::EvtNavigator::adjustPath(const std::vector<std::string>& path)
{
    for (size_t i = 0; i < path.size(); ++i ) {
        std::vector<std::string>::iterator pos = std::find(m_paths.begin(), m_paths.end(),path[i]);
        if (pos == m_paths.end()) {
            // One new path, insert it just for occupying position
            m_paths.insert(m_paths.begin() + i, path[i]);
            m_refs.insert(m_refs.begin() + i, new SmartRef);
            m_writeFlag.insert(m_writeFlag.begin() + i, false);
        }
        else {
            size_t ps = pos - m_paths.begin();
            if (ps != i) {
                // We find the path, but it's on the wrong position
                std::swap(*pos, m_paths[i]);
                std::swap(m_refs[pos - m_paths.begin()], m_refs[i]);
                bool temp = m_writeFlag[pos - m_paths.begin()];
                m_writeFlag[pos - m_paths.begin()] = m_writeFlag[i];
                m_writeFlag[i] = temp;
            }
        }
    }
}

void nEXO::EvtNavigator::setPath(const std::vector<std::string>& paths)
{
    m_paths = paths;
}

bool nEXO::EvtNavigator::writePath(const std::string& path)
{
    std::vector<std::string>::iterator pos = find(m_paths.begin(), m_paths.end(), path);
    if (m_paths.end() == pos) {
        return false;
    }
    return m_writeFlag[pos - m_paths.begin()];
}

void nEXO::EvtNavigator::setWriteFlag(const std::string& path, bool write)
{
    std::vector<std::string>::iterator pos = find(m_paths.begin(), m_paths.end(), path);
    if (m_paths.end() == pos) return;
    m_writeFlag[pos - m_paths.begin()] = write;
}

void nEXO::EvtNavigator::resetWriteFlag()
{
    if (m_writeFlag.size()) m_writeFlag.clear();
    int npath = m_paths.size();
    for (int i = 0; i < npath; ++i) {
        m_writeFlag.push_back(true);
    }
}

const TTimeStamp& nEXO::EvtNavigator::TimeStamp() const
{
    return m_TimeStamp;
}

TTimeStamp& nEXO::EvtNavigator::TimeStamp()
{
    return m_TimeStamp;
}

void nEXO::EvtNavigator::setTimeStamp(const TTimeStamp& value)
{
    m_TimeStamp = value;
}

nEXO::SmartRef* nEXO::EvtNavigator::getSmartRef(const std::string& path)
{
    std::vector<std::string>::iterator pos = std::find(m_paths.begin(), m_paths.end(), path);
    if (m_paths.end() == pos) {
        return 0;
    }
    size_t ps = pos - m_paths.begin();
    if (ps >= m_refs.size()) {
        return 0;
    } 
    return m_refs[ps];
}
