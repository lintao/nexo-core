#include "BookEDM.h"

NEXO_BOOK_EDM(nEXO::GenHeader, nEXO::GenEvent, 100, /Event/Gen);
NEXO_BOOK_EDM(nEXO::SimHeader, nEXO::SimEvent, 200, /Event/Sim);
NEXO_BOOK_EDM(nEXO::MuonSimHeader, nEXO::MuonSimEvent, 210, /Event/MuonSim);
NEXO_BOOK_EDM(nEXO::ElecHeader, nEXO::ElecEvent, 250, /Event/Elec);
NEXO_BOOK_EDM(nEXO::CalibHeader, nEXO::CalibEvent, 300, /Event/Calib);
NEXO_BOOK_EDM(nEXO::RecHeader, nEXO::RecEvent, 400, /Event/Rec);
NEXO_BOOK_EDM(nEXO::RecTrackHeader, nEXO::RecTrackEvent, 410, /Event/RecTrack);
NEXO_BOOK_EDM(nEXO::PhyHeader, nEXO::PhyEvent, 500, /Event/Phy);
NEXO_BOOK_EDM(nEXO::TestHeaderA, nEXO::ATestEventA&nEXO::ATestEventB, 600, /Event/TestA);
NEXO_BOOK_EDM(nEXO::TestHeaderB, nEXO::BTestEventA&nEXO::BTestEventB, 700, /Event/TestB);

NEXO_BOOK_EDM(nEXO::PidTmvaHeader, nEXO::PidTmvaEvent, 1000, /Event/PidTmva);
