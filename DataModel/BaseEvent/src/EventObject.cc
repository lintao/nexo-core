#include "Event/EventObject.h"
ClassImp(nEXO::EventObject);

unsigned int nEXO::EventObject::AddRef() 
{
  return ++m_RefCount;
}

unsigned int nEXO::EventObject::DecRef() 
{
  if (m_RefCount > 0) return --m_RefCount;
  return 0;
}
