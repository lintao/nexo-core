
#include "Event/ElecHeader.h"

ClassImp(nEXO::ElecHeader);

namespace nEXO
{
    ElecHeader::ElecHeader() {

    }

    ElecHeader::~ElecHeader() {

    }

    void ElecHeader::setEventEntry(const std::string& eventName, Long64_t& value) {
        if (eventName == "nEXO::ElecEvent") {
            m_event.setEntry(value);
        }
    }

    EventObject* ElecHeader::event(const std::string& eventName) {
        if (eventName == "nEXO::ElecEvent") {
            return m_event.GetObject();
        }
        return 0;
    }

    bool ElecHeader::hasEvent() {
        return m_event.HasObject();
    }

}
