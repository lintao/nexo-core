#ifndef ElecHeader_h
#define ElecHeader_h

#include "Event/HeaderObject.h"
#include "EDMUtil/SmartRef.h"
#include "Event/ElecEvent.h"
#include <string>

namespace nEXO
{
    class ElecHeader: public HeaderObject
    {
        private:
            nEXO::SmartRef m_event; // ||

        public:
            ElecHeader();
            ~ElecHeader();

            const Long64_t getEventEntry() const {
                return m_event.entry();
            }
            EventObject* event() {
                return m_event.GetObject();
            }
            void setEvent(ElecEvent* value) {
                m_event = value;
            }
            void setEventEntry(const std::string& eventName, Long64_t& value);
            EventObject* event(const std::string& eventName);
            bool hasEvent();

        public:

        public:
            ClassDef(ElecHeader,1)

    };
}

#endif
