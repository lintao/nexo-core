#ifndef ElecChannel_h
#define ElecChannel_h

#include <TObject.h>
#include <vector>

namespace nEXO
{
    class ElecChannel: public TObject {
        private:
            Int_t    fTileId;                    // the tile Id of the hit channel
            Double_t fxTile;                     // the tile X position of the hit channel
            Double_t fyTile;                     // the tile Y position of the hit channel
            Double_t fXPosition;                 // the X position of the hit channel
            Double_t fYPosition;                 // the Y position of the hit channel
            Int_t    fChannelLocalId;            // local Id
            Double_t fChannelCharge;             // charge collected on the hit channel
            Double_t fChannelInductionAmplitude; // charge induction on the hit channel
            Double_t fChannelFirstTime;          // earliest hit time on the channel
            Double_t fChannelLatestTime;         // earliest hit time on the channel
            Double_t fChannelTime;               // hit time on the channel
            Int_t    fChannelNTE;                // true TE number on the channel
            Int_t    fChannelNoiseTag;           // channel noise tag for the channel
            Double_t fInductionAmplitude;        // amplitude of induction signal

            // waveform
            UInt_t   fWFLen;
            Int_t    fWFTileId;
            Int_t    fWFLocalId;
            Double_t fWFChannelCharge;
            std::vector<Double_t> fWFAmplitude;
            std::vector<Double_t> fWFTime;

        public:
            ElecChannel() {
                fTileId = 0;
                fxTile = 0;                    
                fyTile = 0;                    
                fXPosition = 0;                
                fYPosition = 0;                
                fChannelLocalId = 0;           
                fChannelCharge = 0;            
                fChannelInductionAmplitude = 0;
                fChannelFirstTime = 0;         
                fChannelLatestTime = 0;        
                fChannelTime = 0;              
                fChannelNTE = 0;               
                fChannelNoiseTag = 0;          
                fInductionAmplitude = 0;       

                fWFLen = 0;
                fWFTileId = 0;
                fWFLocalId = 0;
                fWFChannelCharge = 0;

            }

            ~ElecChannel() {

            }

        public:
            // NOTE:
            // * getter: 
            //     TYPE XYZ();
            //     const TYPE& XYZ() const;
            // * setter:
            //     void XYZ(TYPE v);
            //     void XYZ(const TYPE& v);

            // getter
            Double_t TileId()                   { return fTileId; }                     
            Double_t xTile()                    { return fxTile; }                     
            Double_t yTile()                    { return fyTile; }                     
            Double_t XPosition()                { return fXPosition; }                 
            Double_t YPosition()                { return fYPosition; }                 
            Int_t    ChannelLocalId()           { return fChannelLocalId; }            
            Double_t ChannelCharge()            { return fChannelCharge; }             
            Double_t ChannelInductionAmplitude(){ return fChannelInductionAmplitude; } 
            Double_t ChannelFirstTime()         { return fChannelFirstTime; }         
            Double_t ChannelLatestTime()        { return fChannelLatestTime; }        
            Double_t ChannelTime()              { return fChannelTime; }               
            Int_t    ChannelNTE()               { return fChannelNTE; }               
            Int_t    ChannelNoiseTag()          { return fChannelNoiseTag; }          
            Double_t InductionAmplitude()       { return fInductionAmplitude; }       
            UInt_t   WFLen()                    { return fWFLen; }
            Int_t    WFTileId()                 { return fWFTileId; }
            Int_t    WFLocalId()                { return fWFLocalId; }
            Double_t WFChannelCharge()          { return fWFChannelCharge; }
            const std::vector<Double_t>& WFAmplitude() const { return fWFAmplitude; }
            const std::vector<Double_t>& WFTime()      const { return fWFTime; }

            // setter
            void TileId                   (Double_t v){ fTileId = v; }                     
            void xTile                    (Double_t v){ fxTile = v; }                     
            void yTile                    (Double_t v){ fyTile = v; }                     
            void XPosition                (Double_t v){ fXPosition = v; }                 
            void YPosition                (Double_t v){ fYPosition = v; }                 
            void ChannelLocalId           (Int_t    v){ fChannelLocalId = v; }            
            void ChannelCharge            (Double_t v){ fChannelCharge = v; }             
            void ChannelInductionAmplitude(Double_t v){ fChannelInductionAmplitude = v; } 
            void ChannelFirstTime         (Double_t v){ fChannelFirstTime = v; }         
            void ChannelLatestTime        (Double_t v){ fChannelLatestTime = v; }        
            void ChannelTime              (Double_t v){ fChannelTime = v; }               
            void ChannelNTE               (Int_t    v){ fChannelNTE = v; }               
            void ChannelNoiseTag          (Int_t    v){ fChannelNoiseTag = v; }          
            void InductionAmplitude       (Double_t v){ fInductionAmplitude = v; }       
            void WFLen                    (UInt_t   v){ fWFLen = v; }
            void WFTileId                 (Int_t    v){ fWFTileId = v; }
            void WFLocalId                (Int_t    v){ fWFLocalId = v; }
            void WFChannelCharge          (Double_t v){ fWFChannelCharge = v; }
            void WFAmplitude(const std::vector<Double_t>& v) { fWFAmplitude = v; }
            void WFTime(const std::vector<Double_t>& v){ fWFTime = v; }
        public:
            ClassDef(ElecChannel, 1)
    };

}

#endif
