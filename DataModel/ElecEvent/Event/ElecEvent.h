#ifndef ElecEvent_h
#define ElecEvent_h

#include "Event/EventObject.h"
#include "Event/ElecChannel.h"
#include <vector>


namespace nEXO
{

    class ElecEvent: public EventObject
    {
        private:
            std::vector<ElecChannel> fElecChannels;

            // don't support the copy constructor
            ElecEvent(const ElecEvent& event);

        public:
            ElecEvent();
            ElecEvent(Int_t evtid);
            ~ElecEvent();

            // setter
            void ElecChannels(const std::vector<nEXO::ElecChannel>& v) { fElecChannels = v; }
            void addElecChannel(const nEXO::ElecChannel& v) { fElecChannels.push_back(v); }
            // getter
            const std::vector<nEXO::ElecChannel>& ElecChannels() const { return fElecChannels; }
            // it is possible to return a reference to the vector, then we can modify it directly.
            std::vector<nEXO::ElecChannel>& ElecChannels() { return fElecChannels; }

            ClassDef(ElecEvent, 1)

    };
}

#endif
