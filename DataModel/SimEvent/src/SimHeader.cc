
#include "Event/SimHeader.h"

ClassImp(nEXO::SimHeader);

namespace nEXO
{
    SimHeader::SimHeader() {

    }

    SimHeader::~SimHeader() {

    }

    void SimHeader::setEventEntry(const std::string& eventName, Long64_t& value) {
        if (eventName == "nEXO::SimEvent") {
            m_event.setEntry(value);
        }
    }

    EventObject* SimHeader::event(const std::string& eventName) {
        if (eventName == "nEXO::SimEvent") {
            return m_event.GetObject();
        }
        return 0;
    }

    bool SimHeader::hasEvent() {
        return m_event.HasObject();
    }

}
