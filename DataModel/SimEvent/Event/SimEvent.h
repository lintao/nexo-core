#ifndef SimEvent_h
#define SimEvent_h

#include "Event/EventObject.h"
#include <vector>


namespace nEXO
{

    class SimEvent: public EventObject
    {
        private:
            int fEventNumber;
            double fGenX;
            double fGenY;
            double fGenZ;
            double fTotalEventEnergy;
            int fNumDeposits;
            std::vector<double> fLengthDeposit;
            std::vector<double> fEnergyDeposit;
            std::vector<double> fPreEnergyDeposit;
            std::vector<double> fPostEnergyDeposit;
            std::vector<int>    fTrackNumber;
            std::vector<double> fXpos;
            std::vector<double> fYpos;
            std::vector<double> fZpos;
            std::vector<double> fTglob;
            std::vector<double> fTloc;
            
            int fNOP;
            int fInitNOP;
            int fInitCherenkovOP;
            std::vector<double> fOPEnergy;
            std::vector<int>    fSiPMID;
            std::vector<double> fOPTime;
            std::vector<int>    fOPType;
            std::vector<int>    fOPStopVolume;
            std::vector<double> fOPX;
            std::vector<double> fOPY;
            std::vector<double> fOPZ;
            std::vector<float>  fGenOPX;
            std::vector<float>  fGenOPY;
            std::vector<float>  fGenOPZ;
            std::vector<int>    fGenOPN;
            std::vector<float>  fGenOPXlist;
            std::vector<float>  fGenOPYlist;
            std::vector<float>  fGenOPZlist;
            
            int fNTE;
            std::vector<double> fTEEnergy;
            std::vector<double> fTEX;
            std::vector<double> fTEY;
            std::vector<double> fTEZ;


            // don't support the copy constructor
            SimEvent(const SimEvent& event);

        public:
            SimEvent();
            SimEvent(Int_t evtid);
            ~SimEvent();

            // List of public interfaces
            // * event number
            int EventNumber() { return fEventNumber; }
            void EventNumber(int v) { fEventNumber = v; }

            // * gen x/y/z
            double GenX() { return fGenX; }
            double GenY() { return fGenY; }
            double GenZ() { return fGenZ; }

            void GenX(double v) { fGenX = v; }
            void GenY(double v) { fGenY = v; }
            void GenZ(double v) { fGenZ = v; }

            // * total event energy
            double TotalEventEnergy() { return fTotalEventEnergy; }
            void TotalEventEnergy(double v) { fTotalEventEnergy = v; }

            // * Deposits
            // TODO it is better to use an object to represet it.
            // ** getters
            int NumDeposits() { return fNumDeposits;}
            const std::vector<double>& LengthDeposit     (){ return fLengthDeposit;     }
            const std::vector<double>& EnergyDeposit     (){ return fEnergyDeposit;     }
            const std::vector<double>& PreEnergyDeposit  (){ return fPreEnergyDeposit;  }
            const std::vector<double>& PostEnergyDeposit (){ return fPostEnergyDeposit; }
            const std::vector<int>   & TrackNumber       (){ return fTrackNumber;       }
            const std::vector<double>& Xpos              (){ return fXpos;              }
            const std::vector<double>& Ypos              (){ return fYpos;              }
            const std::vector<double>& Zpos              (){ return fZpos;              }
            const std::vector<double>& Tglob             (){ return fTglob;             }
            const std::vector<double>& Tloc              (){ return fTloc;              } 
            // ** setters
            void NumDeposits(int v) { fNumDeposits = v;}
            void LengthDeposit     (const std::vector<double>& v){  fLengthDeposit     = v; }
            void EnergyDeposit     (const std::vector<double>& v){  fEnergyDeposit     = v; }
            void PreEnergyDeposit  (const std::vector<double>& v){  fPreEnergyDeposit  = v; }
            void PostEnergyDeposit (const std::vector<double>& v){  fPostEnergyDeposit = v; }
            void TrackNumber       (const std::vector<int>   & v){  fTrackNumber       = v; }
            void Xpos              (const std::vector<double>& v){  fXpos              = v; }
            void Ypos              (const std::vector<double>& v){  fYpos              = v; }
            void Zpos              (const std::vector<double>& v){  fZpos              = v; }
            void Tglob             (const std::vector<double>& v){  fTglob             = v; }
            void Tloc              (const std::vector<double>& v){  fTloc              = v; } 

            // * OP
            // TODO it is better to use an object to represet it.
            // ** getters
            int NOP() { return fNOP; }
            int InitNOP() { return fInitNOP; }
            int InitCherenkovOP() { return fInitCherenkovOP; }
            const std::vector<double>& OPEnergy    () { return fOPEnergy     ; }
            const std::vector<int>   & SiPMID      () { return fSiPMID       ; }
            const std::vector<double>& OPTime      () { return fOPTime       ; }
            const std::vector<int>   & OPType      () { return fOPType       ; }
            const std::vector<int>   & OPStopVolume() { return fOPStopVolume ; }
            const std::vector<double>& OPX         () { return fOPX          ; }
            const std::vector<double>& OPY         () { return fOPY          ; }
            const std::vector<double>& OPZ         () { return fOPZ          ; }
            const std::vector<float> & GenOPX      () { return fGenOPX       ; }
            const std::vector<float> & GenOPY      () { return fGenOPY       ; }
            const std::vector<float> & GenOPZ      () { return fGenOPZ       ; }
            const std::vector<int>   & GenOPN      () { return fGenOPN       ; }
            const std::vector<float> & GenOPXlist  () { return fGenOPXlist   ; }
            const std::vector<float> & GenOPYlist  () { return fGenOPYlist   ; }
            const std::vector<float> & GenOPZlist  () { return fGenOPZlist   ; }
            // ** setters
            void NOP(int v) { fNOP = v; }
            void InitNOP(int v) { fInitNOP = v; }
            void InitCherenkovOP(int v) { fInitCherenkovOP = v; }
            void OPEnergy    (const std::vector<double>& v) { fOPEnergy     = v; }
            void SiPMID      (const std::vector<int>   & v) { fSiPMID       = v; }
            void OPTime      (const std::vector<double>& v) { fOPTime       = v; }
            void OPType      (const std::vector<int>   & v) { fOPType       = v; }
            void OPStopVolume(const std::vector<int>   & v) { fOPStopVolume = v; }
            void OPX         (const std::vector<double>& v) { fOPX          = v; }
            void OPY         (const std::vector<double>& v) { fOPY          = v; }
            void OPZ         (const std::vector<double>& v) { fOPZ          = v; }
            void GenOPX      (const std::vector<float> & v) { fGenOPX       = v; }
            void GenOPY      (const std::vector<float> & v) { fGenOPY       = v; }
            void GenOPZ      (const std::vector<float> & v) { fGenOPZ       = v; }
            void GenOPN      (const std::vector<int>   & v) { fGenOPN       = v; }
            void GenOPXlist  (const std::vector<float> & v) { fGenOPXlist   = v; }
            void GenOPYlist  (const std::vector<float> & v) { fGenOPYlist   = v; }
            void GenOPZlist  (const std::vector<float> & v) { fGenOPZlist   = v; }
            
            // * TE
            // ** getters
            int NTE() { return fNTE; }
            const std::vector<double>& TEEnergy (){ return fTEEnergy; }
            const std::vector<double>& TEX (){ return fTEX; }
            const std::vector<double>& TEY (){ return fTEY; }
            const std::vector<double>& TEZ (){ return fTEZ; }
            // ** setters
            void NTE(int v) { fNTE = v; }
            void TEEnergy(const std::vector<double>& v){ fTEEnergy = v; }
            void TEX(const std::vector<double>& v){ fTEX = v; }
            void TEY(const std::vector<double>& v){ fTEY = v; }
            void TEZ(const std::vector<double>& v){ fTEZ = v; }

            ClassDef(SimEvent, 1)

    };
}

#endif
