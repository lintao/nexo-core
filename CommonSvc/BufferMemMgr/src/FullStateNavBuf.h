#ifndef FULL_STATE_NAV_BUF_H
#define FULL_STATE_NAV_BUF_H

#include "EvtNavigator/NavBuffer.h"

class IInputStream;

class FullStateNavBuf : public nEXO::NavBuffer
{
    public :

        FullStateNavBuf(double lowBound, double highBound);

        virtual ~FullStateNavBuf();

        void init(IInputStream* iStream);

        bool next();

        bool adopt(nEXO::EvtNavigator* nav);

        bool reset(int entry);

    private :

        void trimDated();
        void fillFresh();
        void fillNext();
        double timeShift(nEXO::EvtNavigator* nav);

        int                m_rEntry;
        const double       m_floor;
        const double       m_ceiling;

        nEXO::EvtNavigator*  m_beyond;
        IInputStream*      m_iStream;
};

#endif
