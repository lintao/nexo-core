#ifndef SNIPER_I_DATA_MEM_MGR_H
#define SNIPER_I_DATA_MEM_MGR_H

#include <string>

namespace nEXO {
    class EvtNavigator;
}

class IDataMemMgr
{
    public :

        virtual ~IDataMemMgr() {}

        virtual bool adopt(nEXO::EvtNavigator* nav, const std::string& path) = 0;

        virtual bool reset(const std::string& path, int entry) = 0;
};

#endif
