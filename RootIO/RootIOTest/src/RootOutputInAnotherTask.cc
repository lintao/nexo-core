
#include "SniperKernel/AlgBase.h"
#include "EvtNavigator/NavBuffer.h"
#include "TRandom.h"
#include "SniperKernel/AlgFactory.h"
#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/Incident.h"
#include "DataRegistritionSvc/DataRegistritionSvc.h"
#include "BufferMemMgr/IDataMemMgr.h"
#include "Event/SimHeader.h"
#include "Event/SimEvent.h"



class RootOutputInAnotherTask : public AlgBase
{
    public :
        RootOutputInAnotherTask(const std::string& name);
        virtual ~RootOutputInAnotherTask();

        virtual bool initialize();
        virtual bool execute();
        virtual bool finalize();

    private:
        void generate_hits(nEXO::SimEvent*);
        void generate_trks(nEXO::SimEvent*);

    private :
        int m_totalpe;

        int m_iEvt;
        TRandom m_r;

        Task* iotask;
        std::string iotaskname;
};

DECLARE_ALGORITHM(RootOutputInAnotherTask);


RootOutputInAnotherTask::RootOutputInAnotherTask(const std::string& name)
    : AlgBase(name) 
{
    m_iEvt = 0;
    iotask = 0;
    iotaskname = "detsimiotask";
    declProp("TotalPE", m_totalpe=1000);
}

RootOutputInAnotherTask::~RootOutputInAnotherTask()
{
}

bool RootOutputInAnotherTask::initialize()
{
    Task* toptask = Task::top();
    iotask = dynamic_cast<Task*>(toptask->find(iotaskname));
    if (iotask == 0) {
        LogError << "Can't find the task for I/O." << std::endl;
        throw SniperException("Make sure the IO task is created");
    }
    SniperPtr<DataRegistritionSvc> drsSvc(iotask, "DataRegistritionSvc");
    if ( drsSvc.invalid() ) {
        LogError << "Failed to get DataRegistritionSvc!" << std::endl;
        throw SniperException("Make sure you have load the DataRegistritionSvc.");
    }
    // drsSvc->registerData("nEXO::SimEvent", "/Event/SimEvent");

    return true;

}

bool RootOutputInAnotherTask::execute()
{
    ++m_iEvt;

    //create EvtNavigator, set TimeStamp
    for (int i = 0; i < 2; ++i) {
        nEXO::EvtNavigator* nav = new nEXO::EvtNavigator();
        static TTimeStamp time(2014, 7, 29, 10, 10, 2, 111);
        time.Add(TTimeStamp(0, abs(m_r.Gaus(200000, 200000/5))));
        nav->setTimeStamp(time);

        //put EvtNavigator into data buffer
        SniperPtr<IDataMemMgr> mMgr(iotask, "BufferMemMgr");
        mMgr->adopt(nav, "/Event");

        //set headers and events ...
        // = event =
        nEXO::SimEvent* event = new nEXO::SimEvent();
        // == generate dummy data ==
        generate_hits(event);
        generate_trks(event);

        // = header =
        nEXO::SimHeader* header = new nEXO::SimHeader();
        header->setEvent(event);
        nav->addHeader("/Event/SimEvent", header);

        Incident::fire(iotaskname);
    }

    return true;
}

bool RootOutputInAnotherTask::finalize()
{
    LogInfo << " finalized successfully" << std::endl;

    return true;
}

void RootOutputInAnotherTask::generate_hits(nEXO::SimEvent* event)
{
    // assume there are 1000000 hits

    for (int i = 0; i < m_totalpe; ++i) {
        nEXO::SimPMTHit* jm_hit = event->addCDHit();
        jm_hit -> setPMTID(m_r.Integer(20000));
        jm_hit -> setNPE(m_r.Integer(2000));
        jm_hit -> setHitTime(m_r.Integer(2000));

    }
}

void RootOutputInAnotherTask::generate_trks(nEXO::SimEvent* event)
{

}
