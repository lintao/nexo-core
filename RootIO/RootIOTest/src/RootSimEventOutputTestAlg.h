#ifndef ROOT_SimEvent_OUTPUT_TEST_ALG 
#define ROOT_SimEvent_OUTPUT_TEST_ALG

#include "SniperKernel/AlgBase.h"
#include "EvtNavigator/NavBuffer.h"
#include "TRandom.h"

namespace nEXO {
    class SimEvent;
}

class RootSimEventOutputTestAlg : public AlgBase
{
    public :
        RootSimEventOutputTestAlg(const std::string& name);
        virtual ~RootSimEventOutputTestAlg();

        virtual bool initialize();
        virtual bool execute();
        virtual bool finalize();

    private:
        void generate_hits(nEXO::SimEvent*);
        void generate_trks(nEXO::SimEvent*);

    private :
        int m_totalpe;

        nEXO::NavBuffer* m_buf;
        int m_iEvt;
        TRandom m_r;
};

#endif
