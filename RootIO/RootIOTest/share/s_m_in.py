#!/usr/bin/env python
# -*- coding:utf-8 -*-

import Sniper

#create task
task = Sniper.Task("task")
task.asTop()
task.setLogLevel(0)

#create RootInputSvc and configure input files
import RootIOSvc
task.createSvc("RootInputSvc/InputSvc")
ri = task.find("InputSvc")
ri.property("InputFile").set(["tut_sample3.root", "tut_sample4.root"])

#create Data Buffer Manager
import BufferMemMgr
bufMgr = task.createSvc("BufferMemMgr")
bufMgr.property("TimeWindow").set([0, 0]);

#create algorithm
import RootIOTest
task.property("algs").append("MultiEventInAlg/ialg")

task.setEvtMax(30000)
task.show()
task.run()
