#!/usr/bin/env python
# -*- coding:utf-8 -*-

import Sniper

def configure_iotask(task):
    import DataRegistritionSvc
    dr = task.createSvc("DataRegistritionSvc")
    dr.property("EventToPath").set({"nEXO::SimEvent": "/Event/SimEvent"})
    # == RootIO ==
    import RootIOSvc
    ro = task.createSvc("RootOutputSvc/OutputSvc")
    ro.property("OutputStreams").set({"/Event/SimEvent": "sample_split.root"})
    # == Data Buffer ==
    import BufferMemMgr
    bufMgr = task.createSvc("BufferMemMgr")

task = Sniper.Task("task")
task.asTop()
task.setLogLevel(0)

detsimiotask = task.createTask("Task/detsimiotask")
configure_iotask(detsimiotask)

import RootIOTest
task.property("algs").append("RootOutputInAnotherTask")

task.setEvtMax(10)
task.show()
task.run()

