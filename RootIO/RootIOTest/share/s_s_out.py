#!/usr/bin/env python
# -*- coding:utf-8 -*-

import Sniper
import sys

#create task
task = Sniper.Task("task")
task.asTop()
task.setLogLevel(0)

num = int(sys.argv[1])

#create RootOutputSvc and configure output path and files
import RootIOSvc
task.createSvc("RootOutputSvc/OutputSvc")
ro = task.find("OutputSvc")
ro.property("OutputStreams").set({"/Event/Phy": "tut_sample" + str(num) + ".root"})

#create BufferMemMgr
import BufferMemMgr
bufMgr = task.createSvc("BufferMemMgr")
bufMgr.property("TimeWindow").set([0, 0]);

#create algorithm
import RootIOTest
task.property("algs").append("SingleEventOutAlg/oalg")
oalg = task.find("oalg")
oalg.property("offset").set(num)

task.setEvtMax(1000000)
task.show()
task.run()
