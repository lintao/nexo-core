#!/usr/bin/env python
# -*- coding:utf-8 -*-

import Sniper

#create task
task = Sniper.Task("task")
task.asTop()
task.setLogLevel(0)

#create RootOutputSvc and configure output path and files
import RootIOSvc
task.createSvc("RootOutputSvc/OutputSvc")
ro = task.find("OutputSvc")
ro.property("OutputStreams").set({"/Event/TestA": "tut_sample0.root","/Event/TestB": "tut_sample0.root"})

#create BufferMemMgr
import BufferMemMgr
bufMgr = task.createSvc("BufferMemMgr")
bufMgr.property("TimeWindow").set([0, 0]);

#create algorithm
import RootIOTest
task.property("algs").append("EventOutAlg/oalg")

task.setEvtMax(10000)
task.show()
task.run()
