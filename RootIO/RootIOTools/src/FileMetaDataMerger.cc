#include "FileMetaDataMerger.h"
#include "RootFileInter.h"
#include "FileMetaData.h"
#include "TFile.h"

FileMetaDataMerger::FileMetaDataMerger(std::map<std::string, std::vector<Long64_t> >* breakPoints)
       : IMerger(), m_breakPoints(breakPoints)
{
}

FileMetaDataMerger::~FileMetaDataMerger()
{
}

void FileMetaDataMerger::merge(TObject*& obj, std::string& path, std::string& name)
{
    TFile* file = new TFile(m_inputFiles[0].c_str(), "read");
    nEXO::FileMetaData* ifmd = RootFileInter::GetFileMetaData(file);
    nEXO::FileMetaData* ofmd = new nEXO::FileMetaData(*ifmd);
    ofmd->SetBreakPoints(*m_breakPoints);
    obj = ofmd;
    delete ifmd;
    file->Close();
    delete file;
    path = "Meta";
    name = "FileMetaData";
}
